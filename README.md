
[![Project Status: Active – The project has reached a stable, usable
state and is being actively
developed.](https://www.repostatus.org/badges/latest/active.svg)](https://www.repostatus.org/#active)
[![Signed
by](https://img.shields.io/badge/Keybase-Verified-brightgreen.svg)](https://keybase.io/hrbrmstr)
![Signed commit
%](https://img.shields.io/badge/Signed_Commits-100%25-lightgrey.svg)
[![Linux build
Status](https://travis-ci.org/hrbrmstr/f5wx.svg?branch=master)](https://travis-ci.org/hrbrmstr/f5wx)  
![Minimal R
Version](https://img.shields.io/badge/R%3E%3D-3.6.0-blue.svg)
![License](https://img.shields.io/badge/License-AGPL-blue.svg)

# f5wx

Orchestrate the Hidden API at F5 Weather

## Description

F5 Weather has great data but terrible forecast table aesthetics. Tools
are provided to orchestrate and chart F5 Weather data.

## What’s Inside The Tin

The following functions are implemented:

  - `f5wx_chart`: Make a temperature & precipitation chart from the
    object returned by f5wx\_forecast()
  - `f5wx_forecast`: Retrieve a forecast table from F5 Weather
  - `f5wx_login`: Start a session to F5 Weather

## Installation

``` r
remotes::install_git("https://git.rud.is/hrbrmstr/f5wx.git")
# or
remotes::install_git("https://git.sr.ht/~hrbrmstr/f5wx")
# or
remotes::install_gitlab("hrbrmstr/f5wx")
```

NOTE: To use the ‘remotes’ install options you will need to have the
[{remotes} package](https://github.com/r-lib/remotes) installed.

## Usage

``` r
library(f5wx)

# current version
packageVersion("f5wx")
## [1] '0.1.0'
```

``` r
library(ggplot2)
library(tibble)

f5wx_login()

res <- f5wx_forecast()

res$f_cast
## # A tibble: 10 x 5
##    V1           V2    V3 conditions c_alpha
##    <fct>     <int> <int> <chr>        <dbl>
##  1 Sat 10/3     46    67 Clear         1   
##  2 Sun 10/4     45    68 Clear         1   
##  3 Mon 10/5     52    63 Rain          1   
##  4 Tue 10/6     48    68 Clear         1   
##  5 Wed 10/7     53    61 Rain          0.25
##  6 Thu 10/8     50    56 Clear         1   
##  7 Fri 10/9     41    46 Clear         1   
##  8 Sat 10/10    38    51 Clear         1   
##  9 Sun 10/11    46    60 Clear         1   
## 10 Mon 10/12    50    61 Clear         1

f5wx_chart(res) +
  labs(title = "ECMWF Forecast for Berwick, Maine")
```

<img src="man/figures/README-ex01-1.png" width="750" />

## f5wx Metrics

| Lang | \# Files | (%) | LoC |  (%) | Blank lines | (%) | \# Lines |  (%) |
| :--- | -------: | --: | --: | ---: | ----------: | --: | -------: | ---: |
| R    |        4 | 0.4 | 171 | 0.46 |          31 | 0.3 |       62 | 0.34 |
| Rmd  |        1 | 0.1 |  15 | 0.04 |          20 | 0.2 |       30 | 0.16 |
| SUM  |        5 | 0.5 | 186 | 0.50 |          51 | 0.5 |       92 | 0.50 |

clock Package Metrics for f5wx

## Code of Conduct

Please note that this project is released with a Contributor Code of
Conduct. By participating in this project you agree to abide by its
terms.
